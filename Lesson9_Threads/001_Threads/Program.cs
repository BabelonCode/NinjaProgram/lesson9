﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace _001_Threads
{
    //https://blogs.msdn.microsoft.com/pedram/2007/10/07/a-performance-comparison-of-readerwriterlockslim-with-readerwriterlock/
    //https://docs.microsoft.com/en-us/dotnet/api/system.threading.readerwriterlockslim?view=netframework-4.8
    class Program
    {
        static void Main(string[] args)
        {
            int count = 180;
            var rnd = new Random();
            var worker = new Worker();

            var threads1 = new Thread[count];
            for (int i = 0; i < count; i += 2)
            {
                threads1[i] = new Thread(() => WorkerPrintAction(worker, rnd));
                threads1[i + 1] = new Thread(() => WorkerAddAction(worker, rnd));
            }

            for (int i = 0; i < count; i++)
            {
                threads1[i].Start();
            }

            Console.ReadLine();
        }

        static void WorkerAddAction(Worker worker, Random rnd)
        {
            int id = rnd.Next(1, 5);
            worker.TryAdd(id, id);
        }

        static void WorkerPrintAction(Worker worker, Random rnd)
        {
            int id = rnd.Next(1, 5);
            worker.TryPrint(id);
        }
    }

    class Worker
    {
        public Worker()
        {
            _lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        }

        private readonly ReaderWriterLockSlim _lock;
        private Dictionary<int, int> dic = new Dictionary<int, int>();

        public void TryAdd(int id, int value)
        {
            _lock.EnterWriteLock();

            dic[id] = value;

            _lock.ExitWriteLock();
        }

        public void TryPrint(int id)
        {
            _lock.EnterReadLock();

            if (dic.TryGetValue(id, out int value))
                Console.WriteLine($"[Worker] id = {id}, value = {value}");

            _lock.ExitReadLock();
        }
    }
}
