﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace _005_Threads
{
    static class Program
    {
        //private static Mutex mutex;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            const string appName = "Lesson SingletonApp";
            using (var mutex = new Mutex(false, appName, out bool createdNew))
            {
                if (createdNew)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());
                }
            }
        }
    }
}
