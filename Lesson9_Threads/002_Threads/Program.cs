﻿using System;
using System.Threading;

namespace _002_Threads
{
    //https://www.youtube.com/watch?v=DAwhCr3tS-8
    //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/volatile
    class Program
    {
        //static volatile bool stop; // Без JIT оптимизации.
        static bool stop; // С JIT оптимизацией.

        static void Main()
        {
            Console.WriteLine("Main: run thread in 2 second");
            var thread = new Thread(Worker);
            thread.Start();

            Thread.Sleep(2000);

            stop = true;
            Console.WriteLine("Main: waiting for the thread to complete.");
            thread.Join();

            Console.ReadLine();
        }

        private static void Worker(Object o)
        {
            int x = 0;
            while (!stop)
            {
                x++;
            }

            //// Код после JIT оптимизации, если stop не voatile:
            //// (Если stop - volatile - то оптимизация JIT компилятором производиться не будет.)
            //int x = 0;
            //if (stop != true)
            //{
            //    while (true)
            //    {
            //        x++;
            //    }
            //}

            Console.WriteLine("Worker: x = {0}.", x);
        }
    }
}
