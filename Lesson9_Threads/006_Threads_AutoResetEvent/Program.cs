﻿using System;
using System.Threading;

namespace _006_Threads_AutoResetEvent
{
    class Program
    {
        // Represents a thread synchronization event that, when signaled, resets automatically 
        //after releasing a single waiting thread. This class cannot be inherited.
        static readonly AutoResetEvent auto = new AutoResetEvent(false);

        static void Main()
        {
            Console.WriteLine("Enter any key.\n");

            var thread = new Thread(Function);
            thread.Start();

            Console.ReadKey();
            auto.Set(); // Послать сигнал первому потоку

            Console.ReadKey();
            auto.Set(); // Послать сигнал второму потоку

            // Delay.
            Console.ReadKey();
        }

        static void Function()
        {
            Console.WriteLine("Red light");
            auto.WaitOne(); // после завершения WaitOne() AutoResetEvent автоматически переходит в несигнальное состояние.
            Console.WriteLine("Yellow");
            auto.WaitOne(); // после завершения WaitOne() AutoResetEvent автоматически переходит в несигнальное состояние.
            Console.WriteLine("Green");
        }
    }
}
