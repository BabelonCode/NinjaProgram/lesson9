﻿using System;
using System.Threading;

namespace _007_Threads_ManualResetEvent
{
    class Program
    {
        // ManualResetEvent - Уведомляет один или более ожидающих потоков о том, что произошло событие.
        private static ManualResetEvent manual = new ManualResetEvent(false);

        static void Main()
        {
            Console.WriteLine("Enter any key.\n");

            Thread[] threads = { new Thread(Function1), new Thread(Function2) };

            foreach (Thread thread in threads)
                thread.Start();

            Console.ReadKey();
            manual.Set(); // Просылает сигнал всем потокам.

            // Delay.
            Console.ReadKey();
        }

        static void Function1()
        {
            Console.WriteLine("Task 1 begins and waits for the singnal.");
            manual.WaitOne(); // после завершения WaitOne() ManualResetEvent остаеться в сигнальном сотоянии.
            Console.WriteLine("Task 1 releases.");
        }

        static void Function2()
        {
            Console.WriteLine("Task 2 is running and waits for the singnal.");
            manual.WaitOne(); // после завершения WaitOne() ManualResetEvent остаеться в сигнальном сотоянии.
            Console.WriteLine("Task 2 releases.");
        }
    }
}
