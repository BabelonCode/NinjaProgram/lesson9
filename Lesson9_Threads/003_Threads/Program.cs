﻿using System;
using System.Threading;

namespace _003_Threads
{
    //Create Custom ThreadPool
    //https://www.codeproject.com/Articles/548865/Create-a-Custom-ThreadPool-in-Csharp
    //
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Start Working.");
            ShowThreadInfo();

            Console.WriteLine("Start Thread1");
            ThreadPool.QueueUserWorkItem(new WaitCallback(Task1));
            
            Thread.Sleep(1000);

            Console.WriteLine("Start Thread2");
            ThreadPool.QueueUserWorkItem(Task2);

            Thread.Sleep(1000);

            Console.WriteLine("The Main Thread complated.\n");

            Console.WriteLine("Task1 and Task2 complated.");
            ShowThreadInfo();
            Console.ReadLine();
        }

        static void Task1(object state)
        {
            Thread.CurrentThread.Name = "1";
            Console.WriteLine("ThreadName: {0}, ThreadId: {1}", Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine();
            Thread.Sleep(200);
        }

        static void Task2(object state)
        {
            Thread.CurrentThread.Name = "2";
            Console.WriteLine("ThreadName: {0}, ThreadId: {1}", Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine();
            Thread.Sleep(200);
        }

        static void ShowThreadInfo()
        {
            ThreadPool.GetAvailableThreads(out int availableWorkThreads, out int completionPortThreads);
            ThreadPool.GetMaxThreads(out int maxWorkThreads, out int maxCompletionPortThreads);
            Console.WriteLine("-------------Available Work Threads: {0} from {1}", availableWorkThreads, maxWorkThreads);
            Console.WriteLine("-------------Completion Port Threads:{0} from {1}\n", completionPortThreads, maxCompletionPortThreads);
        }
    }
}